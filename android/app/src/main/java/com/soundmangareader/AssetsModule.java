package com.soundmangareader;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;

public class AssetsModule extends ReactContextBaseJavaModule {
    AssetsModule(ReactApplicationContext context) {
        super(context);

        // This line creates the obb directory if it doesn't exist
        context.getObbDir();
    }

    @Override
    public String getName() {
        return "AssetsModule";
    }
}
