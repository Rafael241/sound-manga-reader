import React, {ReactNode, useEffect, useState} from 'react';
import {useParams} from 'react-router-native';
import {ActivityIndicator, Text} from 'react-native-paper';
import {FileSystem} from 'react-native-file-access';
import {BackHandler} from 'react-native';
import Layout from '../components/Layout';
import {translate} from '../../utils/localize';
import _ from 'lodash';
import {useNavigate} from 'react-router-native';
import {ScriptManifest} from '../../utils/file';
import {SDCardDir} from '../../utils/preferences';

// TODO: Avoid redundancy
async function checkIntegrity(
  script: ScriptManifest,
  packsDir: string,
  manga: string,
) {
  let tmpErrorMessages = [];
  for (const [chapterIndex, chapter] of Object.entries(script.chapters)) {
    for (let i = 0; i < chapter.pages.length; ++i) {
      // Check image
      const fileExists = await FileSystem.exists(
        SDCardDir +
          packsDir +
          '/' +
          manga +
          '/img/ch-' +
          chapterIndex +
          '/' +
          chapter.pages[i].page +
          '.jpg',
      );
      if (!fileExists) {
        tmpErrorMessages.push(
          translate('missing') +
            ': ' +
            packsDir +
            '/' +
            manga +
            '/img/ch-' +
            chapterIndex +
            '/' +
            chapter.pages[i].page +
            '.jpg',
        );
      }

      // Check BGM
      if (chapter.pages[i].bgm) {
        const bgmExists = await FileSystem.exists(
          SDCardDir +
            packsDir +
            '/' +
            script.soundDir +
            '/bgm/' +
            chapter.pages[i].bgm +
            '.ogg',
        );
        if (!bgmExists) {
          // Do not show the same BGM error twice
          if (
            !tmpErrorMessages.includes(
              translate('missing') +
                ': ' +
                packsDir +
                '/' +
                script.soundDir +
                '/bgm/' +
                chapter.pages[i].bgm +
                '.ogg',
            )
          ) {
            tmpErrorMessages.push(
              translate('missing') +
                ': ' +
                packsDir +
                '/' +
                script.soundDir +
                '/bgm/' +
                chapter.pages[i].bgm +
                '.ogg',
            );
          }
        }
      }

      for (const se of chapter.pages[i].se) {
        const seExists = await FileSystem.exists(
          SDCardDir + packsDir + '/' + script.soundDir + '/se/' + se + '.ogg',
        );
        if (!seExists) {
          // Do not show the same BGM error twice
          if (
            !tmpErrorMessages.includes(
              translate('missing') +
                ': ' +
                packsDir +
                '/' +
                script.soundDir +
                '/se/' +
                se +
                '.ogg',
            )
          ) {
            tmpErrorMessages.push(
              translate('missing') +
                ': ' +
                packsDir +
                '/' +
                script.soundDir +
                '/se/' +
                se +
                '.ogg',
            );
          }
        }
      }

      // Check Voice
      if (chapter.pages[i].voice) {
        const voiceExists = await FileSystem.exists(
          SDCardDir +
            packsDir +
            '/' +
            manga +
            '/voice/ch-' +
            chapterIndex +
            '/' +
            chapter.pages[i].page +
            '.ogg',
        );
        if (!voiceExists) {
          tmpErrorMessages.push(
            translate('missing') +
              ': ' +
              packsDir +
              '/' +
              manga +
              '/voice/ch-' +
              chapterIndex +
              '/' +
              chapter.pages[i].page +
              '.ogg',
          );
        }
      }
    }
  }
  return tmpErrorMessages;
}

interface MangaIntegrityViewProps {
  packsDir: string;
}
const MangaIntegrityView = ({packsDir}: MangaIntegrityViewProps) => {
  const navigate = useNavigate();
  const params = useParams();
  const manga = params.manga ? params.manga : '';
  const [title, setTitle] = useState(manga);

  const goToMangaScreen = () => {
    navigate('/manga/' + manga, {replace: true});
    return true;
  };

  useEffect(() => {
    const backPressListener = BackHandler.addEventListener(
      'hardwareBackPress',
      goToMangaScreen,
    );
    return () => {
      backPressListener.remove();
    };
  }, []);

  const [errorMessages, setErrorMessages] = useState<String[]>([]);
  const [loadMessage, setLoadMessage] = useState<ReactNode>(
    <ActivityIndicator size="large" />,
  );

  useEffect(() => {
    FileSystem.readFile(SDCardDir + packsDir + '/' + manga + '/script.json')
      .then(result => {
        const script = JSON.parse(result);
        setTitle(script.title);
        checkIntegrity(script, packsDir, manga).then(tmpErrorMessages => {
          setErrorMessages(tmpErrorMessages);
          setLoadMessage(null);
        });
      })
      .catch(err => {
        setErrorMessages(curErrorMessages => {
          curErrorMessages.push(
            'Error ' +
              err.code +
              ': ' +
              packsDir +
              '/' +
              manga +
              '/script.json ' +
              translate('missing'),
          );
          return curErrorMessages;
        });
        setLoadMessage(null);
      });
  }, [manga]);

  return (
    <Layout title={title} goBack={() => goToMangaScreen()}>
      <Text variant="bodyMedium">{translate('checkIntegrityDescription')}</Text>
      {loadMessage ? (
        <Text variant="bodyMedium">{loadMessage}</Text>
      ) : (
        <>
          <Text variant="titleLarge">{translate('detectedErrors')}</Text>
          {!_.isEmpty(errorMessages) ? (
            <>
              <Text variant="bodyMedium">
                {translate('detectedErrorsDescription')}
              </Text>
              {errorMessages.map((errorMessage, key) => (
                <Text
                  variant="bodyMedium"
                  key={manga + '-integrity-err-' + key}>
                  {errorMessage}
                </Text>
              ))}
            </>
          ) : (
            <Text variant="bodyMedium">{translate('noErrorDetected')}</Text>
          )}
        </>
      )}
    </Layout>
  );
};

export default MangaIntegrityView;
