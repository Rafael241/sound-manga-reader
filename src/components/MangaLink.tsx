import {useNavigate} from 'react-router-native';
import {List} from 'react-native-paper';
import React, {useEffect, useState} from 'react';
import {FileSystem} from 'react-native-file-access';
import {ScriptManifest} from '../../utils/file';
import {
  grantManageExternalStoragePermission,
  SDCardDir,
} from '../../utils/preferences';
import {translate} from '../../utils/localize';

interface MangaLinksProps {
  packsDir: string;
  folder: string;
  appState: string;
}

const MangaLink = ({packsDir, folder, appState}: MangaLinksProps) => {
  const navigate = useNavigate();
  const [script, setScript] = useState<ScriptManifest>();
  const [missingPermission, setMissingPermission] = useState<boolean>();

  const loadManga = () => {
    setMissingPermission(false);
    FileSystem.readFile(SDCardDir + packsDir + '/' + folder + '/script.json')
      .then(result => {
        const jsonScript: ScriptManifest = JSON.parse(result);
        if ('soundDir' in jsonScript) {
          // This ensures we use a compatible script (recently added mandatory key)
          setScript(jsonScript);
        }
      })
      .catch(err => {
        if (err && err.message && err.message.includes('EACCES')) {
          setMissingPermission(true);
        } else if (err && err.message && err.message.includes('ENOENT')) {
          console.log(folder + ' does not contain a script.json, not showing');
        } else {
          console.log(folder + ': unknown error');
        }
      });
  };

  useEffect(() => {
    loadManga();
  }, [folder, appState]);

  return (
    <>
      {script && (
        <List.Item
          title={script.title}
          onPress={() => navigate('/manga/' + folder, {replace: true})}
        />
      )}
      {missingPermission && (
        <List.Item
          title={translate('missingPermission')}
          onPress={async () => {
            await grantManageExternalStoragePermission();
          }}
        />
      )}
    </>
  );
};

export default MangaLink;
