import {useNavigate} from 'react-router-native';
import {List} from 'react-native-paper';
import React, {useEffect, useState} from 'react';
import {FileSystem} from 'react-native-file-access';
import {prettifyMusicFolderName} from '../../utils/file';
import {SDCardDir} from '../../utils/preferences';
import {AppStateStatic, PermissionsAndroid} from 'react-native';

interface MusicBoxLinkProps {
  packsDir: string;
  folder: string;
}

const MusicBoxLink = ({packsDir, folder}: MusicBoxLinkProps) => {
  const navigate = useNavigate();
  const [shown, setShown] = useState(false);

  useEffect(() => {
    FileSystem.exists(SDCardDir + packsDir + '/' + folder + '/bgm/')
      .then(result => {
        if (result) {
          setShown(true);
        }
      })
      .catch(err => {
        console.log(err);
        console.log(folder + ' does not contain a bgm folder, not showing');
      });
  }, [folder]);

  return (
    <>
      {shown && (
        <List.Item
          title={prettifyMusicFolderName(folder)}
          onPress={async () => {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            );

            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              navigate('/musicbox/' + folder, {replace: true});
            }
          }}
        />
      )}
    </>
  );
};

export default MusicBoxLink;
